﻿using UnityEngine;
using System.Collections.Generic;


public class OfflineTeamManagerScript : MonoBehaviour{
    public GameObject[] CharacterList;    
    public bool IsPlayer;
    public GameObject OtherTeam;


    static int LayerCount = 13;
    List<GameObject> spawnedCharacters;

    void Awake(){
        spawnedCharacters = new List<GameObject>();
        SpawnCharacters(false);
    }

    public void SetTeamActive(bool val){
        spawnedCharacters.RemoveAll(item => item == null);

        int index = Random.Range(0, spawnedCharacters.Count);
        for (int i = 0; i < spawnedCharacters.Count; i++){
            var obj = spawnedCharacters[i];
            obj.GetComponent<RoleControlScript>().SetMyTeamActive(val);
            if (!val) obj.GetComponent<RoleControlScript>().SetControlEnable(false);
            else if (i == index){
                 obj.GetComponent<RoleControlScript>().SetControlEnable(true);
            }
        }
    }

    public void DisableOtherMembers(GameObject except){
        spawnedCharacters.RemoveAll(item => item == null);
        foreach(var obj in spawnedCharacters){
            if (except != obj){
                obj.GetComponent<RoleControlScript>().SetControlEnable(false);
            }
        }
    }

    public void SwitchTurn(){
        OtherTeam.GetComponent<OfflineTeamManagerScript>().SetTeamActive(true);
        SetTeamActive(false);
    }

    public void SpawnCharacters (bool above) {
        Vector2[] positions;
        if (IsPlayer){
            positions = new []{ new Vector2(0f, -2.9f), new Vector2(-1.3f, -2f), new Vector2(1.3f, -2f)};
        }
        else {
            positions = new []{ new Vector2(0f, 2.9f), new Vector2(-1.3f, 2f), new Vector2(1.3f, 2f)};
        } 

        for(int i = 0; i < CharacterList.Length; i++){
            var characterPrefab = CharacterList[i];
            var character = Instantiate(characterPrefab);
            character.layer = LayerCount;
            character.transform.SetParent(transform);
            character.GetComponent<FindPathScript>().SetCollidePoint(positions[i]);
            spawnedCharacters.Add(character);
            LayerCount ++;

            if (!IsPlayer){
                character.GetComponent<RoleControlScript>().SetAutomaticControl(true);
            }
        }
        if (IsPlayer){
            SetTeamActive(true);
        }
    }
}
