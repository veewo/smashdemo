﻿using UnityEngine;

public class RoleNetworkViewScript : Photon.MonoBehaviour {
    void OnCollisionExit2D(Collision2D coll){
        // Online
        if (photonView.isOwnerActive){
            if (photonView.isMine){
                DetectHurt(coll);
            }
        } else {
            DetectHurt(coll);
        }

    }

    void DetectHurt(Collision2D coll){
        if (!GetComponent<RoleMoveScript>().IsCurrentActive()){
            if (gameObject.tag == "Player" && coll.gameObject.tag == "Enemy"){
                if (photonView.isOwnerActive) photonView.RPC("BeHurt", PhotonTargets.Others, 20f);
                gameObject.GetComponent<HealthScript>().BeHurt(20f);
            } else if (gameObject.tag == "Enemy" && coll.gameObject.tag == "Player"){
                if (photonView.isOwnerActive) photonView.RPC("BeHurt", PhotonTargets.Others, 20f);
                gameObject.GetComponent<HealthScript>().BeHurt(20f);
            }
        }
    }
    
}
