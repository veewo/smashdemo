﻿using UnityEngine;
using System.Collections.Generic;


public class OnlineTeamManagerScript : Photon.MonoBehaviour{
    public string[] CharacterNameList;    

    static int LayerCount = 13;
    List<GameObject> spawnedCharacters;

    void Awake(){
        spawnedCharacters = new List<GameObject>();
    }

    [RPC]
    public void SetTeamActive(bool val){
        spawnedCharacters.RemoveAll(item => item == null);
        foreach(var obj in spawnedCharacters){
            obj.GetComponent<RoleControlScript>().SetMyTeamActive(val);
        }
    }

    public void DisableOtherMembers(GameObject except){
        spawnedCharacters.RemoveAll(item => item == null);
        foreach(var obj in spawnedCharacters){
            if (except != obj){
                obj.GetComponent<RoleControlScript>().SetControlEnable(false);
            }
        }
    }


    public void SwitchTurn(){
        photonView.RPC("SetTeamActive", PhotonTargets.Others, true);
        SetTeamActive(false);
    }

    public void SpawnCharacters (bool above, string tag) {
        Vector2[] positions;
        if (!above){
            positions = new []{ new Vector2(0f, -2.9f), new Vector2(-1.3f, -2f), new Vector2(1.3f, -2f)};
        }
        else {
            positions = new []{ new Vector2(0f, 2.9f), new Vector2(-1.3f, 2f), new Vector2(1.3f, 2f)};
        } 
        // Vector2[] positions = { new Vector2(0f, -2.9f), new Vector2(-1.3f, -2f), new Vector2(1.3f, -2f)};
        for(int i = 0; i < CharacterNameList.Length; i++){
            var character = PhotonNetwork.Instantiate(CharacterNameList[i], Vector2.zero, Quaternion.identity, 0);
            character.layer = LayerCount;
            character.tag = tag;
            character.transform.SetParent(transform);
            character.GetComponent<FindPathScript>().SetCollidePoint(positions[i]);
            character.GetComponent<Rigidbody2D>().position = character.transform.position;
            spawnedCharacters.Add(character);
            LayerCount ++;
        }
    }
}
