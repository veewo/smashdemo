﻿using UnityEngine;

public class NetworkManager : MonoBehaviour {
    // Use this for initialization
    public GameObject TeamManager;
    bool isCreatedRoom;
    void Start () {
        PhotonNetwork.ConnectUsingSettings("0.1");

        if (!PhotonNetwork.connected){
            PhotonNetwork.JoinLobby();
        }
    }


    // First connection is failed.
    void OnFailedToConnectToPhoton(DisconnectCause cause){
    }

    // Something cause the connection failed.
    void OnConnectionFail(DisconnectCause cause){
    }

    void OnJoinedLobby(){
        Debug.Log("Joined Lobby");
        PhotonNetwork.JoinRandomRoom();
    }

    void OnPhotonRandomJoinFailed(){
        Debug.Log("Can't join random room!");
        PhotonNetwork.CreateRoom(null);
    }

    void OnPhotonCreateRoomFailed(){
        Debug.Log("Creating room failed.");
    }

    void OnCreatedRoom(){
        isCreatedRoom = true;
    }

    void OnJoinedRoom(){
        Debug.Log("Joined room.");
        if (isCreatedRoom){
            TeamManager.GetComponent<OnlineTeamManagerScript>().SpawnCharacters(false, "Player");
            TeamManager.GetComponent<OnlineTeamManagerScript>().SetTeamActive(true);
        } else {
            TeamManager.GetComponent<OnlineTeamManagerScript>().SpawnCharacters(true, "Enemy");
        }
    }
}
