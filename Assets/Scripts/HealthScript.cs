﻿using UnityEngine;

public class HealthScript : MonoBehaviour {
	public float Health = 100;
	private float currentHealth;
	void Awake(){
		currentHealth = Health;
	}
	public float GetHealth(){
		return currentHealth;
	}
	// public void SetHealth(float health){
	// 	currentHealth = health;
	// 	BroadcastMessage("SetPercent", currentHealth/Health, SendMessageOptions.DontRequireReceiver);			
	// }
	[RPC]
	public void BeHurt(float damage){
		currentHealth -= damage;
		if (currentHealth < 0) {
			Destroy(gameObject); 
		} else {
			BroadcastMessage("SetPercent", currentHealth/Health, SendMessageOptions.DontRequireReceiver);			
		}
	}
}
