﻿using UnityEngine;
using System.Collections.Generic;
using Vectrosity;

[RequireComponent (typeof (CircleCollider2D))]
public class FindPathScript : MonoBehaviour
{	
	private VectorLine line;
	private CircleCollider2D circleCollider2D;
	// Just need 4 points.
	private List<Vector3> path;

	void Awake () {		
		circleCollider2D = GetComponent<CircleCollider2D>();
	}
	
	public void DestoryLine(){
		VectorLine.Destroy(ref line);
	}

	public Vector2 GetCollidePoint(){
		return (Vector2)transform.position + circleCollider2D.offset;
	}

	public void SetCollidePoint(Vector2 point){
		transform.position = point - circleCollider2D.offset;
	}
	
	public void FindPath (Vector2 endPosition, Vector2 startPosition) {
		VectorLine.Destroy(ref line);
        if (Vector2.Distance(startPosition, endPosition) > 0.01f) {	
			path = new List<Vector3>();
			// Natural direction.
			Vector2 direction = startPosition - endPosition;
			Vector2 rayFromPoint = circleCollider2D.offset + (Vector2)transform.position;
			path.Add(rayFromPoint);
			RaycastHit2D hit = Physics2D.CircleCast(rayFromPoint, circleCollider2D.radius, direction, Mathf.Infinity, ~( 1 << gameObject.layer));
			// for (int i = 1 ; i <= 2; i++){
			// 	if(!hit.collider){
			// 		Debug.LogError("Ray hit nothing.");
			// 		break;
			// 	}
				// new direction
				direction = Vector3.Reflect(direction, hit.normal);			
				rayFromPoint = hit.centroid;
                path.Add(rayFromPoint);
                hit = Physics2D.CircleCast(rayFromPoint, circleCollider2D.radius, direction, Mathf.Infinity, ~((1 << gameObject.layer | 1 << hit.collider.gameObject.layer)));								
			// }			
			// last point
			path.Add(hit.centroid);

			line = new VectorLine("IndicatorLine", path.ToArray(), null, 2f, LineType.Continuous, Joins.Weld);			
			line.Draw();			
		}		
	}
}