﻿using UnityEngine;
using System.Collections;

public class ShowBloodScript : MonoBehaviour {
	private Transform bloodbar;
	void Awake(){		
		bloodbar = transform.GetChild(0);
		SetPercent(1);
	}		
	public void SetPercent(float percent){		
		if (percent == 1) {
//			gameObject.SetActive(false);			
//			bloodbar.gameObject.SetActive(false);
		} else{			
//			gameObject.SetActive(true);
//			bloodbar.gameObject.SetActive(true);
//			float xOffset = -(1 - percent)/4;
//			bloodbar.Translate(xOffset, 0, 0, bloodbar);
			bloodbar.localScale = new Vector3(percent, 1);		
		}
	}
	
}
