﻿using UnityEngine;

[RequireComponent (typeof (CircleCollider2D))]
[RequireComponent (typeof (Rigidbody2D))]
public class RoleMoveScript : MonoBehaviour {	
	private Rigidbody2D rigidBody2D;
	private float originalMass;
	private bool isCurrentActive;
	public float _forceFactor = 10f;
	private RoleControlScript roleControlScript;

	void Awake(){
		rigidBody2D = GetComponent<Rigidbody2D>();
		originalMass = rigidBody2D.mass;
		roleControlScript = GetComponent<RoleControlScript>();
	}

	void FinishTurn(){
		isCurrentActive = false;
		rigidBody2D.mass = originalMass;
		roleControlScript.SetControlEnable(false);
		SendMessageUpwards("SwitchTurn");
	}

	public bool IsCurrentActive (){
		return isCurrentActive;
	}

	void Update(){
		if (rigidBody2D.velocity.sqrMagnitude < 1f){
			rigidBody2D.velocity = new Vector2(0, 0);
			if (isCurrentActive){
				FinishTurn();
			}
		}
	}

	public void StartMove(Vector2 direction){
		Debug.Log("Start move");
		Vector2 force = Vector3.Normalize(direction) * _forceFactor;
		rigidBody2D.mass = 1f;
		rigidBody2D.AddForce(force, ForceMode2D.Impulse);
		isCurrentActive = true;
	}
}
