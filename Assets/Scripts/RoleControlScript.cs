﻿using UnityEngine;
using System.Collections.Generic;

public class RoleControlScript : MonoBehaviour {
	public GameObject ActiveIndicatorPrefab;

	Vector2? startPosition;
	Vector2? endPosition;
	bool isControlEnabled;
	bool isMyTeamActive;
	
	RoleMoveScript roleMoveScript;
	FindPathScript findPathScript;
	bool automaticControl;

	GameObject activeIndicator;

	void Start(){
		findPathScript = GetComponent<FindPathScript>();
		roleMoveScript = GetComponent<RoleMoveScript>();
	}

	public void SetAutomaticControl(bool val){
		automaticControl = val;

	}

	public void SetMyTeamActive(bool val){
		isMyTeamActive = val;
	}

	public void SetControlEnable(bool enable){
		isControlEnabled = enable;
		if (enable){
			activeIndicator = Instantiate(ActiveIndicatorPrefab);
			activeIndicator.transform.SetParent(transform);
			activeIndicator.transform.position = transform.position + (Vector3)GetComponent<Collider2D>().offset;
		} else {
			if (activeIndicator) {
				Destroy(activeIndicator);
			}
		}
	}
	void ReadyToMove(Vector2 startPoint, Vector2 endPoint){
		findPathScript.DestoryLine();
		roleMoveScript.StartMove(startPoint - endPoint);
		SetControlEnable(false);
	}
	void FindPath(Vector2 startPoint, Vector2 endPoint){
		findPathScript.FindPath(endPoint, startPoint);
	}

	void OnMouseDown(){
		if (isMyTeamActive){
			if (!isControlEnabled) {
				SendMessageUpwards("DisableOtherMembers", gameObject, SendMessageOptions.RequireReceiver);
				SetControlEnable(true);
			}
		}
	}
	void Update () {
		if (!isControlEnabled) {
			return;
		}

		if (automaticControl){
			Debug.Log("automatic");
			endPosition = findPathScript.GetCollidePoint();
			var player = GameObject.FindWithTag("Player");
			if (player){
				startPosition = (player.GetComponent<Collider2D>()).offset + (Vector2)player.transform.position;
				ReadyToMove(startPosition.Value, endPosition.Value);
			}
		} else{
			if (Input.touchCount > 0) {
				Touch touch = Input.GetTouch(0);
				switch(touch.phase) {
				case TouchPhase.Began:
					startPosition = touch.position;				
					break;
				case TouchPhase.Moved:
					if (startPosition.HasValue) {
						endPosition = touch.position;
						FindPath(startPosition.Value, endPosition.Value);
					} else {
						endPosition = null;
					}				
					break;
				case TouchPhase.Ended:
					if (endPosition.HasValue && startPosition.HasValue) {					
						ReadyToMove(startPosition.Value, endPosition.Value);
					}
					endPosition = null;
					startPosition = null;
					break;
				}		
			} else {
				if (Input.GetMouseButtonDown(0)){
					startPosition = Input.mousePosition;
					endPosition = null;				
				}
				
				if (Input.GetMouseButton(0) && startPosition.HasValue){
					if (startPosition.HasValue){
						endPosition = Input.mousePosition;
						if (Vector2.Distance(endPosition.Value, startPosition.Value) > 2){
							FindPath(startPosition.Value, endPosition.Value);
						}
					} else {
						endPosition = null;
					}
				}
				
				if (Input.GetMouseButtonUp(0)){				
					if (startPosition.HasValue && endPosition.HasValue){
						if (Vector2.Distance(endPosition.Value, startPosition.Value) > 2){
							ReadyToMove(startPosition.Value, endPosition.Value);				
						}
					}
					startPosition = null;
					endPosition = null;
				}
			}

		}
	}
}
